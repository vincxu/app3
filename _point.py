from math import sqrt
import random

random.seed()


def distance(p1, p2):
    return sqrt(pow(p2['y'] - p1['y'], 2) + pow(p2['x'] - p1['x'], 2))


def generate_point(numb):
    list = []
    for i in range(0, numb):
        x = random.randint(0, 100)
        y = random.randint(0, 100)
        list.append({'x':x,'y':y,'name':str(i)})
    return list


def path_distance(list_point):
    res = 0
    for i in range(0, len(list_point) - 1):
        res += distance(list_point[i], list_point[i+1])
    return res
        
    
def print_point(list_point):
    for el in list_point:
        print el['name']
