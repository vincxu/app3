from copy import copy
from  _point import distance, generate_point, path_distance

def path(points):
    remaining = copy(points)
    list = []

    start = remaining.pop(0)
    list.append(start)
    while len(remaining) > 0:
        dist = -1

        for i in range(0, len(remaining)):
            new_dist = distance(remaining[i], start)
            if new_dist < dist or dist == -1:
                dist = new_dist
                index = i
        start = remaining.pop(index)
        list.append(start)

    return list

def plot(list_point, fig):
    x = []
    y = []
    for el in list_point:
        x.append(el['x'])
        y.append(el['y'])
    
    sub_greedy = fig.add_subplot(211)
    sub_greedy.plot(x, y, marker='o', linestyle='--', color='r')
    sub_greedy.axis([-4, max(x) + 4, -4, max(y) + 4])
    
