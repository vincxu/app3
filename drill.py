import matplotlib.pyplot as plt
import _greedy
import _point
import _aprox
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("nb_point",  help="number of points", type=int)
parser.add_argument("strategy", help="g for greedy, a for approximation, ag for both")
parser.add_argument("-p", "--plot", help="plot points, greedy solution and spawing tree", action="store_true")
args = parser.parse_args()


list_point = _point.generate_point(args.nb_point)


if args.plot:
    greedy_sol = _greedy.path(list_point)
    fig = plt.figure(1)
    _greedy.plot(greedy_sol, fig)
    for el in list_point:
        plt.annotate("%s" %el['name'], xy=(el['x'], el['y']), xytext=(3,3), textcoords='offset points')
    sub_aprox = fig.add_subplot(212)
    aprox = _aprox.prism(list_point)
    _aprox.plot_graph(aprox, sub_aprox)
    # sub_aprox.axis([-4, max(x) + 4, -4, max(y) + 4])
    for el in list_point:
        plt.annotate("%s" %el['name'], xy=(el['x'], el['y']), xytext=(3,3), textcoords='offset points')
    fig.savefig('fig.png')
    plt.show()


if args.strategy == "g":
    greedy_sol = _greedy.path(list_point)
    print "greedy sol : " + str(_point.path_distance(greedy_sol))
elif args.strategy == "a":
    aprox_sol = _aprox.path(list_point)
    print "aprox sol : " + str(_point.path_distance(aprox_sol))
elif args.strategy == "ag":
    greedy_sol = _greedy.path(list_point)
    aprox_sol = _aprox.path(list_point)
    print "greedy sol : " + str(_point.path_distance(greedy_sol))
    print "aprox sol : " + str(_point.path_distance(aprox_sol))
    
# _point.print_point(aprox_sol)
