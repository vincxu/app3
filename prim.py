import sys
from copy import copy, deepcopy
from _point import distance
from bheap import BHeap

def mst_heap(points):
    remaining_vert = deepcopy(points)
    if len(remaining_vert) < 2:
        print 'list must contain at least 2 points'
        return []

    h = BHeap(4096)
    for i in range(0, len(remaining_vert)):
        h.insert(remaining_vert[i], sys.maxint)

    edges = []
    tree_vert = []
    while h.get_size() > 0:
        node = h.extract()
        tree_vert.append(node)

        if 'edge' in node:
            edges.append(node['edge'])

        current_list = h.get_list()
        for i in range(0, len(current_list)):
            current_node = current_list[i]
            d = distance(current_node, node)
            if d < current_node['key']:
                current_node['edge'] = [
                    {'x': node['x'], 'y': node['y'], 'name': node['name']},
                    {'x': current_node['x'], 'y': current_node['y'], 'name': current_node['name']}
                ]
                h.decrease_key(current_node['id'], d)
    return (tree_vert, edges)

def mst_naive(points):
    remaining_vert = deepcopy(points)
    if len(remaining_vert) < 2:
        print 'list must contain at least 2 points'
        return []

    edges = []
    tree_vert = [remaining_vert.pop(0)]

    while len(remaining_vert) > 0:
        min_dist = sys.maxint # something big
        next_edge = None
        index_to_remove = None
        for i in range(0, len(tree_vert)):
            for j in range(0, len(remaining_vert)):
                d = distance(tree_vert[i], remaining_vert[j])
                if d < min_dist:
                    min_dist = d
                    next_edge = [tree_vert[i], remaining_vert[j]]
                    index_to_remove = j

        tree_vert.append(remaining_vert.pop(index_to_remove))
        edges.append(next_edge)
    return edges


# p1 = {'x': 1, 'y': 2}
# p2 = {'x': 2, 'y': 2}
# p3 = {'x': 0, 'y': 1}
# p4 = {'x': 1, 'y': 3}
# p5 = {'x': 3, 'y': 3}
# p6 = {'x': 1, 'y': 0}
# l = [p1, p2, p3, p4, p5, p6]
# print mst_heap(l)
# print ''
# print mst_naive(l)

# from random import random
# from time import time
#
# points = []
# size = 1000
# for i in range(0, size):
#     points.append({'x': random() * 100, 'y': random() * 100})
#
# st = time()
# mst_h(points)
# et = time()
# print 'heap version: {0}'.format(et - st)
#
# st = time()
# mst_naive(points)
# et = time()
# print 'naive version: {0}'.format(et - st)
