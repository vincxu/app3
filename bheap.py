class BHeap:
    def __init__(self, max_size):
        self.list = [None] * max_size
        self.size = 0

    def get_size(self):
        return self.size

    def get_list(self):
        return self.list[0:self.size]

    def inspect(self):
        print self.list[0:self.size]

    def insert(self, node, key):
        id = self.size
        node['key'] = key
        node['id'] = id
        self.list[self.size] = node
        self.size = self.size + 1

        pid = (id + 1) / 2 - 1
        while id != 0 and self.list[id]['key'] < self.list[pid]['key']:
            self.list[id]['id'] = pid
            self.list[pid]['id'] = id
            self.list[id], self.list[pid] = self.list[pid], self.list[id]
            id = pid
            pid = (id + 1) / 2 - 1

    def extract(self):
        if self.size == 0:
            return None

        ret = self.list[0]
        self.list[0] = self.list[self.size - 1]
        self.list[0]['id'] = 0
        self.size = self.size - 1

        id = 0
        stable = False
        nid = (id + 1) * 2 - 1
        while not stable and nid < self.size:
            if self.list[nid + 1]['key'] < self.list[nid]['key']:
                nid = nid + 1

            if self.list[id]['key'] > self.list[nid]['key']:
                self.list[id]['id'] = nid
                self.list[nid]['id'] = id
                self.list[id], self.list[nid] = self.list[nid], self.list[id]
                id = nid
                nid = (id + 1) * 2 - 1
                continue
            stable = True
        # print '>>> DEBUG'
        # self.inspect()
        return ret

    def decrease_key(self, i, new_key):
        if i >= self.size:
            return

        node = self.list[i]
        if new_key >= node['key']:
            return

        node['key'] = new_key
        id = i
        pid = (id + 1) / 2 - 1
        while id != 0 and self.list[id]['key'] < self.list[pid]['key']:
            self.list[id]['id'] = pid
            self.list[pid]['id'] = id
            self.list[id], self.list[pid] = self.list[pid], self.list[id]
            id = pid
            pid = (id + 1) / 2 - 1


# h = BHeap(32)
# h.insert('a', 4)
# h.insert('b', 2)
# h.insert('c', 10)
# h.insert('d', 7)
# h.insert('e', 9)
# h.insert('f', 5)
# h.insert('g', 8)
#
# h.inspect()
# h.decrease_key(3, 1)
# h.inspect()

# print(h.extract())
# print(h.extract())
# print(h.extract())
# print(h.extract())
# print(h.extract())
# print(h.extract())
# print(h.extract())
# print(h.extract())
# print(h.extract())
# print(h.extract())
