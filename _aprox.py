from copy import copy
from  _point import distance, generate_point, path_distance
from prim import mst_heap

def prism(points):
    remaining = copy(points)
    list = []
    edge = []
    vertice = []
    graph = {}

    for i in range(0, len(remaining)):
        graph[remaining[i]['name']] = {}
        graph[remaining[i]['name']]['node'] = remaining[i]
        graph[remaining[i]['name']]['value'] = []

    current = remaining.pop(0)
    list.append(current)

    while len(remaining) > 0:
        dist = -1
        for i in range(0, len(remaining)):
            for j in range (0, len(list)):
                new_dist = distance(remaining[i], list[j])
                if new_dist < dist or dist == -1:
                    dist = new_dist
                    index = i
                    node = list[j]
        current = remaining.pop(index)
        list.append(current)
        graph[node['name']]['value'].append(current)
        node_name = node['name']
        current_name = current['name']
        if node_name < current_name:
            edge.append([node, current])
        else:
            edge.append([current, node])
        for point in [node, current]:
            if not point in vertice:
                vertice.append(point)

    return (vertice, edge)


def print_graph(graph):
    (vertice, edge) = graph
    for value in edge:
        print str(value[0]) + " : " + str(value[1])

def plot_graph(graph, plt):
    (vertice, edge) = graph
    for value in edge:
        plt.plot([value[0]['x'], value[1]['x']], [value[0]['y'], value[1]['y']], marker='o', linestyle=':', color='b')


def path(list_point):
    graph = prism(list_point)
    # graph = mst_heap(list_point)
    (vertice, edge) = copy(graph)
    remaining = copy(list_point)
    res = []
    previous = {}
    current = remaining.pop(0)
    res.append(current)
    while len(remaining) > 0:
        dist = -1
        to_take = current
        for value in edge:
            for i in range(0, len(value)):
                el = value[i]
                node = value[(i + 1) % len(value)]
                if (el['name'] == current['name']):
                    new_dist = distance(node, current)
                    if new_dist < dist or dist == -1:
                        dist = new_dist
                        to_take = node
                        to_remove = value
        if to_take == current:
            current = previous[current['name']]
            res.append(current)
        else:
            res.append(to_take)
            previous[to_take['name']] = current
            current = to_take
            if to_take in remaining: remaining.remove(to_take)
            if to_remove in edge: edge.remove(to_remove)

    return res
